const express = require("express");
const router = express.Router();
const courseControllers = require("../controllers/courseControllers.js");
const auth =require("../auth.js");

// 1. Refractor the "course" route to implement user authentication for the admin when creating a course.

//creating a course

//___________________________


router.post("/newCourse", (request, response)=>{
	productControllers.addProduct(request.body).then(resultFromController =>response.send(
			resultFromController))
});



//___________________________
router.post("/create", (request, response)=>{
	courseControllers.addProduct(request.body).then(resultFromController =>response.send(
			resultFromController))
});

router.patch("/:productId/archive", auth.verify, (request,response) => 
{
	const newData = {
		product: request.body, 	//request.headers.authorization contains jwt
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	productControllers.archiveProduct(request.params.courseId, newData).then(resultFromController => {
		response.send(resultFromController)
	}	)
})